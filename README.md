*********************************************
       Carsten Rieger IT-Services
        https://www.c-rieger.de
*********************************************

# Script de Instalacion para Nextcloud
Instala tu propio servidor Nextcloud en menos de 10 minutos.<br>

* <b>Ubuntu 20.04</b> LTS [AMD x86_64] / <b>22.04 LTS</b> [AMD x86_64] / <b>Debian 11.x</b> [AMD x86_64]
<br>Soporte de Proxmox LXC<b><sup>new</sup></b><br>
<small>(Tenga en cuenta que Debian requiere una opción de Proxmox llamada 'nesting=1')</small><br>
* NGINX 1.21 from nginx
* Selección / Option: PHP 8.1 // PHP 8.0 // PHP 7.4
* Selección / Option: MariaDB // postgreSQL
* Selección / Option: Self-signed // Let's Encrypt certificates
* Selección / Option: Nextcloud Release 24.x<br>
  <small>(Las versiones de Nextcloud inferiores a 24 no son compatibles con PHP 8.1, configure su version de php!)</small>
* Selección / Option: Nextcloud Office/OnlyOffice
* etc...

<h2>INSTALACION (Ubuntu/Debian):</h2>
<h3>Preparativos:</h3>
<code>sudo -s</code><br>
<code>apt install -y git</code><br>
<code>git clone https://codeberg.org/daire/nextcloud-zero</code><br>
<code>cp nextcloud-zero/zero.sh .</code><br>
<code>chmod +x zero.sh</code><br> <br>
<h3>D/E: Modificar las variables de configuración:</h3></code>
<code>nano zero.sh</code><br> <br>
<code>NEXTCLOUDDATAPATH="/data"</code><br>
<code>NEXTCLOUDADMINUSER="nc_admin"</code><br>
<code>NEXTCLOUDADMINUSERPASSWORD=$(openssl rand -hex 16)</code><br>
<code>NCRELEASE="latest.tar.bz2"</code><br>
<code>PHPVERSION="8.1"</code><br>
<code>NEXTCLOUDDNS="dominio.duckdns.org"</code><br>
<code>LETSENCRYPT="n"</code><br>
<code>NEXTCLOUDEXTIP=$(dig +short txt ch whoami.cloudflare @1.0.0.1)</code><br>
<code>MARIADBROOTPASSWORD=$(openssl rand -hex 16)</code><br>
<code>DATABASE="m"</code><br>
<code>NCDBUSER="ncdbuser"</code><br>
<code>NCDBPASSWORD=$(openssl rand -hex 16)</code><br>
<code>CURRENTTIMEZONE='Europe/Madrid'</code><br>
<code>PHONEREGION='ES'</code><br>
<code>NEXTCLOUDOFFICE="n"</code><br>
<code>ONLYOFFICE="n"</code><br>

<h3>Instalacion:</h3>
<code>./zero.sh</code>
<h2>DESINSTALAR:</h2>
Si quieres volver a ejecutar el script, por favor, desinstálalo primero:<br>
<code>/home/*benutzer*/Nextcloud-Installationsskript/uninstall.sh</code><br>
<code>rm -f /home/*benutzer*/Nextcloud-Installationsskript/uninstall.sh</code><br> <br>
Eliminar todos los datos, bases de datos y software de la instalación anterior. Después podrá volver a ejecutar el script de instalación.<br>
<h2>INSTALACIÓN/REINSTALACIÓN:</h2>
<code>./zero.sh</code><br>
<h2>/LOGFILE:</h2>
<code>nano /home/*benutzer*/Nextcloud-Installationsskript/install.log</code><br>

-----------------------------------------------------------------------------------

Puede encontrar más información sobre el endurecimiento, la optimización y la mejora aquí:<br>&nbsp;<br>
https://www.c-rieger.de/nextcloud-installationsanleitung/<br>&nbsp;<br>
Carsten Rieger IT-Services
